package tw.davidlee.demo.microservice.sso.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


@Component
public class SecurityUserService implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * 密碼模式 身分驗證
     * // 查询数据库操作
     * // 用户角色也应在数据库中获取
     * // 线上环境应该通过用户名查询数据库获取加密后的密码
     * **/
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        if( !"admin".equals(s) )
            throw new UsernameNotFoundException("使用者" + s + "不存在" );
        UserDetails user = new User( s, passwordEncoder.encode("admin") , AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_NORMAL,ROLE_MEDIUM"));
        return user;
    }
}
