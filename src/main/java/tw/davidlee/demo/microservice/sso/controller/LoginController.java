package tw.davidlee.demo.microservice.sso.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import tw.davidlee.demo.microservice.sso.bo.SecurityUserBO;

import java.security.Principal;

@Slf4j
@Controller
public class LoginController {

    @Autowired
    private OAuth2AuthorizedClientService authorizedClientService;

    private SecurityContext context = SecurityContextHolder.getContext();

    /**
     * 登入頁面
     * */
    @GetMapping(value = "/formLogin")
    public ModelAndView formLogin(@RequestParam(value = "error", required = false) String error) {
        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Invalid username and password!");
        }
        model.setViewName("login");
        return model;
    }

    /**
     * 點擊 上方 GOOGLE FB 任一種登入方式
     * */
    @GetMapping(value = "/oauth2Login/{type}")
    public String oauth2Login(@PathVariable String type) {
        return "redirect:/oauth2/authorization/"+type;
    }




//    @RequestMapping(value = "/loginByUserInfo", method = RequestMethod.GET )
//    public String loginByUserInfo(Model model) {
//        return "index";
//    }
//
    /**
     * 首頁
     * */
//    @GetMapping(value = "/index")
//    public String loginByGoogle(Model model , OAuth2AuthenticationToken authentication) {
//        OAuth2AuthorizedClient authorizedClient = this.getAuthorizedClient(authentication);
//        model.addAttribute("userName", authentication.getName());
//        model.addAttribute("clientName", authorizedClient.getClientRegistration().getClientName());
//        return "index";
//    }

    /**
     * formlogin 首頁
     * */
    @GetMapping(value = "/index")
    public ModelAndView index() {
        Object object = context.getAuthentication().getPrincipal();
        ModelAndView model = new ModelAndView();
        if( object instanceof DefaultOAuth2User) {
            DefaultOAuth2User defaultOAuth2User = (DefaultOAuth2User)object;
            model.addObject("userName", defaultOAuth2User.getName());
            model.addObject("loginBy", "google oauth2");
        }else if (object instanceof UserDetails){
            UserDetails userDetails = (UserDetails)object;
            model.addObject("userName", userDetails.getUsername());
            model.addObject("loginBy", "user/password");
        }
        model.setViewName("index");
        return model;
    }

    /**
     * 管理者介面
     * */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @GetMapping(value = "/console")
    public String console(Model model) {
        return "index";
    }

    private OAuth2AuthorizedClient getAuthorizedClient(OAuth2AuthenticationToken authentication) {
        return this.authorizedClientService.loadAuthorizedClient(
                authentication.getAuthorizedClientRegistrationId(), authentication.getName());
    }
}
