package tw.davidlee.demo.microservice.sso.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import tw.davidlee.demo.microservice.sso.handler.LoginAuthenticationFailureHandler;
import tw.davidlee.demo.microservice.sso.handler.LoginAuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true) //开启security注解
public class ServerSecurityConfig extends WebSecurityConfigurerAdapter{

    /**
     * 管理登入驗證
     * */
    @Autowired
    private UserDetailsService securityUserService;

    @Autowired
    private LoginAuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    private LoginAuthenticationFailureHandler authenticationFailureHandler;

    /**
     * 管理Session
     * */
    @Autowired
    SessionRegistry sessionRegistry;
    @Bean
    public SessionRegistry getSessionRegistry(){
        SessionRegistry sessionRegistry=new SessionRegistryImpl();
        return sessionRegistry;
    }

    /**
     * 認證/授權 配置
     * Session併發
     * */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable()
                .authorizeRequests()
                .antMatchers( "/oauth/**","/login","/formLogin","/oauth2Login/**","/error**","/css/**", "/static/**").permitAll()
                .anyRequest().authenticated()
                .and().formLogin()
                        .loginPage("/formLogin")
                        .loginProcessingUrl("/login")
                        .usernameParameter("username")
                        .passwordParameter("password")
                        .failureUrl("/formLogin?error")
                .and().logout()
                        .logoutUrl("/logout")
                        .logoutSuccessUrl("/formLogin")
                .and().oauth2Login()
                        .loginPage("/oauth2Login")
                .and().sessionManagement()
                        .maximumSessions(1)
                        .sessionRegistry(sessionRegistry);

    }


    @Override
    @Bean
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(securityUserService);
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        authenticationProvider.setHideUserNotFoundExceptions(false);
        return authenticationProvider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }
}
